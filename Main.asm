.MEMORYMAP
SLOTSIZE $4000
DEFAULTSLOT 0
SLOT 0 $0000
.ENDME

.ROMBANKMAP
BANKSTOTAL 1
BANKSIZE $4000
BANKS 1
.ENDRO

.BANK 0 SLOT 0
.ORGA 27000

START:
	di							; Disable basic system affecting
	exx							; the keyboard scanning
	push hl						; Preserver the HL' register pair
	exx							; Pop back before return

AGAIN:
	call INIT					; Initialisation
	call TFCTRL					; Traffic control routine
	call RESPC					; Restore underneath
	call MOVTRF					; Move traffic
	call POLICE					; Police car routine
	call FROG 					; Frog module
	call CALSCR					; Calculate and display score
	call SIREN					; Siren or delay
	ld a, (GAMFLG)				; Finish when no frog
	and a
	jr nz, CONTIN
	call OVER					; Highscore management
	jr AGAIN					; New game again

CONTIN:
	ld a, $7f					; Trap space key pressed
	in a, ($fe)					; Scan keyboard
	and 1
	jr nz, MOVE
	call FINAL					; Reset screen and border colour
	exx
	pop hl 						; Retrieve HL'
	exx
	ei							; Enable interrupts
	ret 						; Return to basic system

;********************************************
; FROGDB/ASM
;********************************************
FRGSHP:
.DW FROG1, FROG2, FROG3, FROG4

; Up Frog
FROG1:
.DB 111, 15, 31, 159, 220, 216, 120, 48
.DB 246, 240, 248, 249, 59, 27, 30, 12
.DB 0, 1, 35, 37, 111, 79, 223, 255
.DB 0, 128, 196, 164, 246, 242, 251, 255

; Right Frog
FROG2:
.DB 31, 31, 31, 127, 252, 193, 113, 56
.DB 254, 244, 248, 240, 192, 156, 240, 192
.DB 56, 113, 193, 252, 127, 31, 31, 31
.DB 192, 240, 156, 192, 240, 248, 244, 254

; Down Frog
FROG3:
.DB 255, 223, 79, 111, 37, 35, 1, 0
.DB 255, 251, 242, 246, 164, 196, 128, 0
.DB 48, 120, 216, 220, 159, 31, 15, 111
.DB 12, 30, 27, 59, 249, 248, 240, 246

; Left Frog
FROG4:
.DB 127, 47, 31, 15, 3, 57, 15, 3
.DB 240, 240, 248, 254, 63, 131, 142, 28
.DB 3, 15, 57, 3, 15, 31, 47, 127
.DB 28, 142, 131, 63, 254, 248, 240, 240

LBIKE:
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 31, 63, 115, 81, 169, 112, 112, 32
.DB 254, 252, 252, 234, 213, 206, 14, 4
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 1, 3, 1, 0, 3, 4, 14, 31
.DB 128, 192, 192, 224, 224, 112, 119, 255
.DB 0, 0, 0, 0, 0, 0, 0, 0

LBATT:
.DB 0, 7, 7, 0
.DB 0, 7, 7, 0

RBIKE:
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 127, 63, 63, 87, 171, 115, 112, 32
.DB 248, 252, 206, 138, 149, 14, 14, 4
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 1, 3, 3, 7, 7, 14, 238, 255
.DB 128, 192, 128, 0, 192, 32, 112, 248
.DB 0, 0, 0, 0, 0, 0, 0, 0

RBATT:
.DB 0, 7, 7, 0
.DB 0, 7, 7, 0

LCAR:
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 3, 7, 15, 2, 0, 0
.DB 7, 255, 255, 159, 111, 247, 240, 96
.DB 128, 255, 255, 255, 255, 254, 0, 0
.DB 240, 254, 255, 159, 111, 246, 240, 96
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 63, 97, 193
.DB 0, 0, 0, 0, 0, 0, 128, 192
.DB 0, 0, 0, 0, 0, 0, 0, 0

LCATT:
.DB 0, 6, 6, 6, 6, 0
.DB 0, 0, 0, 6, 6, 0

RCAR:
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 15, 127, 255, 249, 246, 111, 15, 6
.DB 1, 255, 255, 255, 255, 127, 0, 0
.DB 224, 255, 255, 249, 246, 239, 15, 6
.DB 0, 0, 192, 224, 240, 64, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 1, 3
.DB 0, 0, 0, 0, 0, 252, 134, 131
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0

RCATT:
.DB 0, 2, 2, 2, 2, 0
.DB 0, 2, 2, 0, 0, 0

LTRUCK:
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 31, 31, 31, 62, 61, 59, 3, 1
.DB 248, 252, 254, 127, 184, 216, 192, 128
.DB 255, 255, 255, 255, 6, 15, 15, 6
.DB 255, 255, 255, 0, 0, 0, 0, 0
.DB 255, 255, 255, 0, 0, 0, 0, 0
.DB 255, 255, 255, 0, 6, 15, 15, 6
.DB 254, 254, 254, 4, 50, 122, 122, 48
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 7, 9, 17, 17, 31, 31
.DB 2, 2, 250, 250, 254, 252, 252, 248
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 254, 254, 254, 254, 254, 254, 254, 254
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 254, 254, 254
.DB 0, 0, 0, 0, 0, 0, 0, 0

LTATT:
.DB 0, 3, 3, 5, 5, 5, 5, 5, 0
.DB 0, 3, 3, 5, 5, 5, 5, 5, 0
.DB 0, 0, 0, 5, 5, 5, 5, 5, 0

RTRUCK:
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 127, 127, 127, 32, 76, 94, 94, 12
.DB 255, 255, 255, 0, 96, 240, 240, 96
.DB 255, 255, 255, 0, 0, 0, 0, 0
.DB 255, 255, 255, 0, 0, 0, 0, 0
.DB 255, 255, 255, 255, 96, 240, 240, 96
.DB 31, 63, 127, 254, 29, 27, 3, 1
.DB 248, 248, 248, 124, 188, 220, 192, 128
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 127, 127, 127, 127, 127, 127, 127, 127
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 255, 255, 255, 255, 255, 255, 255, 255
.DB 64, 64, 95, 95, 127, 63, 63, 31
.DB 0, 0, 224, 144, 136, 136, 248, 248
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 127, 127, 127
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 255, 255, 255
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0
.DB 0, 0, 0, 0, 0, 0, 0, 0

RTATT:
.DB 0, 5, 5, 5, 5, 5, 3, 3, 0
.DB 0, 5, 5, 5, 5, 5, 3, 3, 0
.DB 0, 5, 5, 5, 5, 5, 0, 0, 0

BLANK:
.DB 0, 0, 0, 0

FRGSTR:
.DS 36											; (4 * 8) + 4

PCSTR:
.DS 120											; (12 * 8) + 12 + 7


;********************************************
; DATABASE (Variables)
;********************************************

OB1EXT:
.DB 0											; Object 1 existence
.DB 0											; Cycle count
.DB 0											; Direction, 0 => right
.DB 0											; Object 1 pos real/abs
.DW 0											; Position counter
.DW 0											; Shape pointer
.DW 0											; Attribute pointer
.DB 0											; Row counter
.DB 0											; Column pointer

OB2EXT:
.DB 0, 0, 0, 0
.DW 0											; Object 2 pos real/abs flag
.DW 0
.DW 0
.DB 0, 0

OB3EXT:
.DB 0, 0, 0, 0
.DW 0
.DW 0
.DW 0
.DB 0, 0

OB4EXT:
.DB 0, 0, 0, 0
.DW 0
.DW 0
.DW 0
.DB 0, 0

OB5EXT:
.DB 0, 0, 0, 0
.DW 0
.DW 0
.DW 0
.DB 0, 0

OB6EXT:
.DB 0, 0, 0, 0
.DW 0
.DW 0
.DW 0
.DB 0, 0

PCAREXT:
.DB 0											; Police car database
PCARCYC:
.DB 0
PCARDIR
.DB 0
PCARRAP:
.DB 0
PCARPOS:
.DW 0
PCARSHP:
.DW 0
PCARATT:
.DW 0
PCARROW:
.DB 2
PCARCOL:
.DB 6

FRGEXT:
.DB 0											; Frog database
FRGCYC:
.DB 0
FRGDIR:
.DB 0											; 0=Up, 1=Right, 2=Down, 3=Left
FRGPOS:
.DW 0
FROGSH:
.DW 0
FRGATR:
.DB 0

FRGDB:
.DB 8, 8, 1

FRGSTN:
.DW $50ac										; Initial position of frog
.DW FROG1
.DB 4											; Attr. total 8 chars

DBINDEX:
.DW RBDB										; Right bike database
.DW LBDB										; Left bike database
.DW RCDB										; Right car database
.DW LCDB										; Left car database
.DW RTDB										; Right truck database
.DW LTDB										; Left truck database

RBDB:
.DB 2, 1, 0, 0									; EXT, CNT, DIR, RAF
.DW $481d										; POS
.DW RBIKE										; Right bike
.DW RBATT										; Attribute
.DB 2, 4										; ROW, COL

LBDB:
.DB 2, 1, 1, 1
.DW $48df
.DW LBIKE
.DW LBATT
.DB 2, 4

RCDB:
.DB 3, 1, 0, 0
.DW $481b
.DW RCAR
.DW RCATT
.DB 2, 6

LCDB:
.DB 3, 1, 1, 1
.DW $48df
.DW LCAR
.DW LCATT
.DB 2, 6

RTDB:
.DB 6, 1, 0, 0
.DW $4818
.DW RTRUCK
.DW RTATT
.DB 3, 9

LTDB:
.DB 6, 1, 1, 1
.DW $48df
.DW LTRUCK
.DW LTATT
.DB 3, 9

LPCDB:
.DB 1, 1, 1, 1
.DW $48df
.DW LCAR
.DW LPCATT
.DB 2, 6

LPCATT:
.DB 0, 5, 5, 5, 5, 0
.DB 0, 0, 0, 5, 5, 0

RPCDB:
.DB 1, 1, 0, 0
.DW $481b
.DW RCAR
.DW RPCATT
.DB 2, 6

RPCATT:
.DB 0, 5, 5, 5, 5, 0
.DB 0, 5, 5, 0, 0, 0

PCTON1:
.DB 41, 0, $f0, 1								; First police car tone

PCTON2:
.DB 23, 0, $bc, 3								; Second police car tone

HOMTON:
.DB $46, 0, $C7, 4								; Frog reach home tone
.DB $5d, 0, $8c, 3
.DB $7c, 0, $a1, 2
.DB $aa, 0, $f1, 1
.DB $de, 0, $6d, 1
.DB $28, 1, 9, 1
.DB $8b, 1, $bf, 0
.DB $0f, 2, $88, 0
.DB $c0, 2, $5e, 0

DIETON:
.DB $84, 3, $43, 0								; Frog dying tone; Reverse

SCRMS1:
.DB "Score"

SCORE:
.DB $30, $30, $30, $30, $30, $30

SCRMS2:
.DB "HIGH SCORE"

HISCR:
.DB $30, $30, $30, $30, $30

IMAGE:
.DSB 5, 0										; Printing image of score
UPDWN:
.DB 0											; Set when the frog moves up or down

COLUMN:
.DB 0											; Variable storing shape column
ROW:
.DB 0											; Variable storing shape row
SKIP:
.DB 0											; Char skipping during draw
FILL:
.DB 0											; Char draw
ATTPOS:
.DW 0											; Holding the attribute file PTR
ATTR:
.DB 0											; Attr of character block
DRWPOS:
.DW 0											; Draw position
STRPOS:
.DW 0											; Store position

ATTPTR:
.DW 0
NEWPOS:
.DW 0											; New traffic object position
POSPTR:
.DW 0											; Traffic position database PTR
GENFLG:
.DB 0											; Traffic regeneration flag

JAMFLG:
.DB 0											; Set to 1 as traffic move jam

CHASE:
.DB 0											; Set when police car appears
SOUNDF:
.DB 0											; Set when user want siren sound
TONFLG:
.DB 0											; Determine which siren tone
RND:
.DW 0											; Pointer to row for random number

GAMFLG:
.DB 1											; End if zero
OLDFRG:
.DW 0											; Old frog pos
NEWFRG:
.DW 0											; New frog pos
CRHFLG:
.DB 0											; Set to 1 when frog was crash
TEMDIR:
.DB 0											; Frog temporary new direction
TEMPOS:
.DW 0											; Frog temporary new position
TEMSHP
.DW 0											; Frog temporary new shape

.DEFINE BOTHY1 $5020
.DEFINE BOTHY2 $5120
.DEFINE TOPHY1 $46A0
.DEFINE TOPHY2 $47A0
.DEFINE MIDHY1 $4B60
.DEFINE MIDHY2 $4C60

.DEFINE CHRSET $3C00							; First 256 bytes nothing

NUMFRG:
.DB 5											; Number of frog

INIT:
	xor a 										; 000 for d2, d1, d0
	out ($fe), a 								; Set border colour
	ld (23624), a 								; to black
	ld (CRHFLG), a
	ld (FRGEXT), a 								; Set frog non exist
	inc a
	ld (GAMFLG), a 								; Set game flag
	ld a, 5										; Initialise frog number
	ld (NUMFRG), a
	ld a, r 									; Generate random PTR
	and $3f										; for this cycle
	ld h, a 									; PTR points to row
	ld a, r
	ld l, a
	ld (RND), hl
	ld hl, $50ac								; Init frog station
	ld (FRGSTN), hl
	call CLRSCR									; Clear screen routine
	call DRWHWY									; Draw highway
	call LINEUP									; Line up all exist frogs
	ld hl, $4000								; Message location
	ld de, SCRMS1								; Load score message
	ld b, 6
	call DISASC									; Display ASCII CHARACTER
	ld hl, SCORE+1								; Print score
	call SCRIMG									; Convert to printable image
	ld hl, $4006
	ld de, IMAGE
	ld b, 5
	call DISASC
	ld hl, $400e								; High score message
	ld de, SCRMS2
	ld b, 11
	call DISASC
	ld hl, HISCR
	call SCRIMG
	ld hl, $4019
	ld de, IMAGE
	ld b, 5
	call DISASC
	ld hl, OB1EXT								; Set all obj nonexist
	ld de, 12
	ld b, 7
	xor a
INTLP1:
	ld (hl), a
	add hl, de
	djnz INTLP1
	ld (CHASE), a 								; Set no police car chase
	inc a
	ld (SOUNDF), a 								; Set siren on
	ld hl, SCORE 								; Initialise score to
	ld de, SCORE+1								; ASCII zero ie $30
	ld c, 5
	ld (hl), $30
	ldir										; Init score to $30
	ret

DRWHWY:
	ld hl, $40a0								; Fill top highway
	call FILHWY
	ld hl, $4860								; Fill middle highway
	call FILHWY
	ld hl, $5020								; Fill bottom highway
	call FILHWY
	ld hl, TOPHY1								; Reverse built highway
	ld de, TOPHY2
	xor a
	call HIGHWY
	ld hl, BOTHY1
	ld de, BOTHY2
	call HIGHWY
	ld hl, MIDHY1
	ld de, MIDHY2
	ld a, 195									; Binary %11000011
HIGHWY:
	ld b, 32									; 32*8 bits
HWYLOP:
	ld (hl), a
	ld (de), a
	inc hl
	inc de
	djnz HWYLOP
	ret

FILHWY:
	ld a, $ff
	exx
	ld b, 32
FILHYL:
	exx
	push hl
	ld b, 8
FILCHR:
	ld (hl), a
	inc h
	djnz FILCHR
	pop hl
	inc hl
	exx
	djnz FILHYL
	exx
	ret

;********************************************
; LINEUP
;********************************************
LINEUP:
	ld a, 1										; Right frog
	ld (FRGDIR), a
	ld de, FROG2								; Right frog shape
	ld hl, (FRGSTN)								; Frog station
	ld a, 4										; (Paper 0)*8+(Ink 4)
	ld (ATTR), a
	ld a, (NUMFRG)								; Number of frog
	and a 										; Test for no frog left
	ret z
	ld b, a 									; Number of frog times
DRAWLN:
	push bc
	push de
	push hl
	call DRWFRG									; Draw frog routine
	pop hl
	pop de
	dec hl
	dec hl
	dec hl
	pop bc
	djnz DRAWLN
	ret

;********************************************
; Draw frog (similar to DRAW routine)
;********************************************
DRWFRG:
	ld a, 2										; Two row frog shape
	ex af, af'
	push hl 									; Store pos PTR
FRGLPO:
	push hl
	ld c, 2										; Column count
FRGLP1:
	push hl
	ld b, 8										; Draw character
FRGLP2:
	ld a, (de)
	ld (hl), a
	inc de
	inc h 										; Next byte of the char
	djnz FRGLP2
	pop hl 										; Current pointer
	inc hl 										; Move to next char pos
	dec c 										; Decrement column count
	jr nz, FRGLP1
	pop hl 										; Row PTR
	ex af, af'
	dec a 										; Decrement lines of char
	ld c, 32
	jr z, FRGATT								; Load frog attribute
	ex af, af'
	and a
	sbc hl, bc 									; Move 32 char/1 line up
	bit 0, h 									; Test cross SCR section
	jr z, FRGLPO
	ld a, h
	sub 7										; Up one screen section
	ld h, a
	jr FRGLPO
FRGATT:
	pop hl 										; Pos PTR
	ld a, h 									; Convert to attribute PTR
	and $18
	sra a
	sra a
	sra a
	add a, $58
	ld h, a
	ld a, (ATTR)								; Fill frog shape ATTR
	ld (hl), a
	inc hl 										; Next character
	ld (hl), a
	sbc hl, bc 									; One line up
	ld (hl), a
	dec hl 										; Next char left
	ld (hl), a
	ret

;********************************************
; TFCTRL
; Traffic control routine
;********************************************
TFCTRL:
	ld hl, GENFLG								; Check regeneration flag
	xor a
	cp (hl)
	jr z, GENER 								; If zero, test generate
	dec (hl)									; Decrement generation flag
	ret
GENER:
	ld hl, OB1EXT								; Start of traffic DB
	ld de, 12									; 12 byte database
	ld b, 6										; 6 DB pairs
TCTRLP:
	cp (hl)										; Test existence
	jr nz, NSPACE
	call REGEN 									; Regeneration routine
	ret
NSPACE:
	add hl, de
	djnz TCTRLP
	ret

;********************************************
; Regen
;
; Regeneration of traffic.
; Input: HL=>DB pairs
;********************************************
REGEN:
	push hl
RAND1:
	call RANDNO									; Random number routine
	and 7										; Generate random number
	cp 6										; From 0 to 5
	jr nc, RAND1
	ld bc, $5921								; Two char test
	ld hl, $5920								; Test jam
	bit 0, a 									; Odd number is left
	jr z, RTRAF									; Right traffic
	ld l, $df
	ld c, $de
RTRAF:
	add a, a 									; Get DBINDEX PTR in DE
	ld e, a
	ld a, (bc)									; Test 2 char ahead
	add a, (hl)
	and a 										; Zero paper; zero ink
	jr z, LOADDB								; If 0, initialise new object
	pop hl 										; If jam, return
	ret
LOADDB:
	ld d, a 									; A=0
	ld hl, DBINDEX 								; Get DB
	add hl, de
	ld e, (hl)									; Get CORR database
	inc hl
	ld d, (hl)
	ex de, hl 									; Source
	pop de 										; Destination
	ld bc, 12
	ldir
	ld a, 2										; Set generation flag
	ld (GENFLG), a 								; Skip for 2 cycles
	ret

;********************************************
; MOVTRF
;
; Move traffic routine
;********************************************
MOVTRF:
	exx
	ld hl, OB1EXT
	ld de, 12
	ld b, 6
MTRFLP:
	push hl
	exx
	pop hl 										; Existence
	ld a, (hl)									; Skip when no exist
	and a
	jp z, NXTMOV
	inc hl 										; Cycle count
	dec (hl)									; Decrement cycle count
	jp nz, NXTMOV
	inc hl 										; Direction
	ld a, (hl)									; 0 = left to right; 1 = right to left
	inc hl
	inc hl
	ld (POSPTR), hl 							; Pos PTR
	ld e, (hl)									; Restore pos
	inc hl
	ld d, (hl)
	inc e 										; Move right
	and a
	jr z, LDPOS
	dec e 										; Move left
	dec e 										; Move left
LDPOS:
	ld (NEWPOS), de
	ex af, af'
	ld bc, 5									; Restore object length
	add hl, bc
	ld a, (hl)									; Row
	ld (ROW), a
	inc hl
	ld a, (hl)									; Column
	ld (COLUMN), a
	dec a
	ld c, a
	ex af, af'
	and a 										; Test direction
	ex de, hl
	jr nz, RTOL									; Right to left
	add hl, bc 									; Find head of truck
	ld a, l 									; LOB
	cp $40										; Test right edge
	jr nc, MOVEOK								; Skip test ahead if off
	jr TESTAH									; Test ahead
RTOL:
	ld a, l 									; New pos. Ahead as well
	cp $c0 										; Test left edge
	jr c, MOVEOK								; Skip test ahead
TESTAH:
	ld a, h 									; Convert to ATTR
	and $18
	sra a
	sra a
	sra a
	add a, $58
	ld h, a
	ld bc, 32
	xor a
	ld (JAMFLG), a 								; Initialise JAM flag
	ld a, (ROW)
TAHLOP:
	ex af, af'
	ld a, (hl)									; Retrieve ATTR
	and 7
	jr z, TFROG1								; Jump if black ink
	cp 4										; Test for green frog
	jr nz, JAM1									; Jam if not a frog
	ld a, 1										; Move if it is a frog
	ld (CRHFLG), a 								; Set frog crash
	jr TFROG1
JAM1:
	ld (JAMFLG), a 								; Set JAM flag non zero
TFROG1:
	and a
	sbc hl, bc
	ex af, af'
	dec a 										; Update row
	jr nz, TAHLOP
	ld a, (JAMFLG)								; Test traffic jam
	and a
	jr z, MOVEOK								; Move if no jam
	exx											; Else stop move one cycle
	inc hl
	inc (hl)									; Load 2 to cycle count
	inc (hl)
	dec hl
	exx
	jr NXTMOV
MOVEOK:
	ld hl, (POSPTR)								; Retrieve PTR to POS
	ld de, (NEWPOS)
	ld (hl), e 									; Store NEWPOS in DB
	inc hl
	ld (hl), d
	call MVCTRL									; Movement control
NXTMOV:
	exx
	add hl, de
	dec b
	jp nz, MTRFLP
	exx
	ret

;********************************************
; MVCTRL
;
; Traffic movement control routine
;********************************************
MVCTRL:
	dec hl
	dec hl 					; DE => NEWPOS; HL => DB PTR
	ld a, e 				; LOB POS
	and $1f					; Test edge
	jr nz, CHGRAF			; Change real ABS flag
	ld a, (hl)
	inc a
	and 1
	ld (hl), a
CHGRAF:
	dec hl 					; PT DIR
	ld a, (hl)
	and a
	jr nz, TOLEFT			; Right to left
	ld a, e
	and $1f					; If to right and ABS
	jr nz, DRWOBJ
	inc hl 					; Get RAF
	ld a, (hl)
	dec hl 					; PT to DIR
	and a 					; If abstract, dies
	jr nz, DRWOBJ
	exx
	ld (hl), a 				; Set non existence
	exx
	ret
TOLEFT:
	ld a, (COLUMN)
	ld c, a
	ex de, hl 				; Test end of object
	add hl, bc 				; Touches left edge
	ld a, l
	cp $c0
	ex de, hl
	jr nz, DRWOBJ
	exx						; Object nonexists as
	ld (hl), 0				; it moves off screen
	exx
	ret
DRWOBJ:
	exx
	ld a, (hl)
	inc hl
	ld (hl), a 				; Refill cycle count
	dec hl
	exx
	inc hl
	push hl 				; PT to RAF
	inc hl
	inc hl
	inc hl
	ld e, (hl)				; Retrieve SHAPE PTR
	inc hl
	ld d, (hl)
	inc hl
	ld c, (hl)				; Retrieve ATTR PTR
	inc hl
	ld b, (hl)
	ld (ATTPTR), bc
	inc hl
	ld a, (hl)
	ld (ROW), a
	inc hl
	ld a, (hl)
	ld (COLUMN), a
	pop hl
	ld a, (hl)				; RAFLAG
	ld hl, (NEWPOS)
	call DRAW
	ret

;********************************************
; DRAW
;
; INPUT 	:	HL=>Start of display pos
;				DE=>PTR to SHAPE DB
;				A =>Position real/abstract flag
;				C =>Number of columns to be displayed
;				COL pass as var
;
; Var 			COLUMN, ROW, ATTR, DRWPOS
;				SKIP, FILL
;
; REG 		:	A, BC, DE, HL, A'
;********************************************
DRAW:
	call RSHAPE				; Return ROW/COL ATTPTR
	ld a, (ROW)
	ex af, af'
LPO:
	push de
	push hl 				; Store line PTR
	ld a, (SKIP)
	ld c, a
	ld b, 0
	add hl, bc 				; Skip POS PTR
	add a, a 				; Multiple of 8 bytes
	add a, a
	add a, a
	ld c, a 				; Skip SHAPE PTR
	ex de, hl
	add hl, bc
	ex de, hl
	bit 0, h 				; Cross screen section?
	jr z, NOSKIP
	ld a, 7					; If yes, Move up
	add a, h
	ld h, a
NOSKIP:
	ld a, (FILL)
	and a
	jr z, NXT
	ld c, a 				; Column to be filled
LP1:
	push hl 				; Fill character
	ld b, 8
LP2:
	ld a, (de)				; Fill character
	ld (hl), a
	inc de
	inc h
	djnz LP2
	pop hl
	dec c
	jr z, NXT
	inc hl 					; Next character
	jr LP1
NXT:
	ex af, af'
	pop hl 					; Restore line PTR
	pop de 					; SHAPE DB PTR
	dec a 					; Update row count
	jr z, LDATTR
	ex af, af'
	and a 					; Clear carry
	ld c, $20
	sbc hl, bc 				; One line up
	bit 0, h 				; Cross screen section?
	jr z, MODDB
	ld a, h
	sub 7
	ld h, a
MODDB:
	ld a, (COLUMN)
	add a, a
	add a, a
	add a, a 				; Update SHAPE DB
	ld c, a
	ex de, hl
	add hl, bc
	ex de, hl
	jr LPO
LDATTR:
	ld hl, (ATTPOS)
	ld de, (ATTPTR)
	ld a, (ROW)
ATROW:
	ex af, af'
	push de
	push hl
	ld a, (SKIP)
	ld c, a
	ld b, 0
	add hl, bc 				; Skip attribute file
	ex de, hl
	add hl, bc 				; Skip attribute database
	ex de, hl
	ld a, (FILL)
	and a
	jr z, SKIPAT			; Skip attribute
	ld b, a 				; Fill attributes
ATTR2:
	ld a, (de)
	ld (hl), a
	inc hl
	inc de
	djnz ATTR2
SKIPAT:
	pop hl
	pop de
	ld a, (COLUMN)
	and a 					; Clear carry
	ld c, $20
	sbc hl, bc 				; Next attribute line up
	ld c, a
	ex de, hl
	add hl, bc 				; Update attribute DB
	ex de, hl
	ex af, af'
	dec a
	jr nz, ATROW
	ret

;********************************************
; RSHAPE
;
; INPUT 	:	HL=>Position
;				A =>Real/abstract flag
;				DE=>Shape PTR
;				COLUMN
;
; OUTPUT 		SKIP, FILL, ATTPOS
;********************************************
RSHAPE:
	push hl
	ex af, af'				; Real shape
	ld h, $1f
	ld a, h
	and l 					; Trap lower 5 bits
	ld l, a
	ld a, h
	sub l 					; Subtract from $1fß
	inc a
	and h 					; Adjust for zero diff
	ld l, a
	ex af, af'
	and a 					; 0=>Abstract; 1=>Real
	ld a, (COLUMN)
	jr nz, REAL
	sub l
	ld (FILL), a
	ld a, l 				; Reload ABS diff
	ld (SKIP), a
	jr CALATT
REAL:
	cp l 					; Take min of col/fill
	jr c, TOOBIG			; Fill more than col
	ld a, l
	and a
	jr nz, TOOBIG
	ld a, (COLUMN)
TOOBIG:
	ld (FILL), a
	xor a
	ld (SKIP), a
CALATT:
	pop hl 					; Calculate ATT PTR
	push hl
	ld a, h
	and $18
	sra a
	sra a
	sra a
	add a, $58
	ld h, a
	ld (ATTPOS), hl
	pop hl
	ret

CLRSCR:
	ld hl, $4000 			; HL=>Start of screen
	ld de, $4001
	ld bc, 6143				; Size of screen $17ff
	xor a 					; Blank screen
	ldir
	ld hl, $5800			; Set first line for score
	ld de, $5801			; of attribute file
	ld bc, 31
	ld (hl), 7				; Ink seven
	ldir
	ld hl, $5820			; Set attribute
	ld de, $5821			; Start of second line
	ld bc, 735
	ld (hl), a 				; (Paper 0)*8 + (Ink 0)
	ldir
	ld hl, $58a0			; Set highway
	ld de, $5960			; High, Middle, Bottom
	ld bc, $5a20
	ld a, 56				; (Paper 7)*8 + (Ink 0)
	exx
	ld b, 32				; Fill one line
HWYATT:
	exx
	ld (hl), a
	ld (de), a
	ld (bc), a
	inc hl
	inc de
	inc bc
	exx
	djnz HWYATT
	exx
	ret

SHAPE:
	push hl 				; Save HL PTR
	ld a, (FRGDIR)
	add a, a
	ld hl, FRGSHP
	ld d, 0
	ld e, a
	add hl, de 				; PTR to POS of shape
	ld e, (hl)				; DE return shape PTR
	inc hl
	ld d, (hl)
	pop hl
	ret

;********************************************
; DISASC
;
; Display ASCII value from character set
; NB: ---- store DE, the message pointer
;		   HL stays the same after display
;		   used BC register as well
;********************************************
DISASC:
	push bc
	push de
	push hl
	ld a, (de)				; Load ASCII char
	ld l, a
	ld h, 0
	add hl, hl 				; Multiple of 8 bytes
	add hl, hl
	add hl, hl
	ex de, hl
	ld hl, CHRSET			; Start of character set
	add hl, de
	ex de, hl
	pop hl
DRWCHR:
	ld b, 8					; Draw character
	push hl
CHARLP:
	ld a, (de)
	ld (hl), a
	inc de
	inc h
	djnz CHARLP
	pop hl
	pop de
	inc hl 					; POS PTR
	inc de 					; Message PTR
	pop bc
	djnz DISASC
	ret

POLICE:
	exx
	ld hl, PCAREXT
	ld a, (hl)				; Test police car exist
	push hl
	exx
	and a
	jr nz, MOVPC			; Move police car
	pop de 					; DB EXT PTR
	call RANDNO				; Move when multiple of
	and $1f 				; 31
	cp $1f
	ret nz
	ld a, 1					; Set chase flag
	ld (CHASE), a
	ld hl, RPCDB			; Right PC
	call RANDNO
	and 1
	jr z, RHTPC
	ld hl, LPCDB
RHTPC:
	ld bc, 12
	ldir
	exx
	push hl
	exx
MOVPC:
	pop hl 					; Existence PTR
	inc hl
	inc hl 					; Direction
	ld a, (hl)
	ld b, a 				; Store dir
	inc hl
	inc hl 					; POSPTR
	ld (POSPTR), hl
	ld e, (hl)
	inc hl
	ld d, (hl)
	inc e 					; Assume move right
	and a
	jr z, PCMRHT			; Police car move right
	dec e
	dec e
PCMRHT:
	ld (NEWPOS), de
	ld a, 2					; Two row
	ld (ROW), a
	ld a, 6
	ld (COLUMN), a
	push bc 				; Direction
	ld a, (PCARRAP)			; Real/ABS flag
	ex de, hl
	call RSHAPE				; Ret SKIP/FILL, ATTR
	ld hl, (ATTPOS)
	pop af
	and a 					; If 1, OK
	jr nz, PCTAH			; Police car test ahead
	ld bc, 5
	add hl, bc
PCTAH:
	ld a, (hl)
	and 7
	ld bc, 32
	and a
	sbc hl, bc
	cp 4
	jr z, ISFRG2
	ld a, (hl)
	and 7
	cp 4
	jr nz, NFROG2
ISFRG2:
	ld a, 1
	ld (CRHFLG), a 			; Set crash flag
	dec a 					; Blank colour
	ld (hl), a 				; Blank fron of PC
	add hl, bc
	ld (hl), a 				; ** Should blank front *
NFROG2:
	call STRPC				; Store new underneath
	ld hl, (POSPTR)
	ld de, (NEWPOS)
	ld (hl), e
	inc hl
	ld (hl), d
	call MVCTRL
	exx						; If non-exist
	ld a, (hl)
	ld (CHASE), a
	exx
	ret

;********************************************
; STRPC
;
; Store underneath police car
;********************************************
STRPC:
	ld hl, (NEWPOS)			; POS PTR
	ld de, PCSTR			; Storage LOC
	ex de, hl
	ld (hl), e				; Store position
	inc hl
	ld (hl), d
	inc hl
	ex de, hl
	ld hl, ROW				; Load 5 bytes of info
	ld a, (hl)
	ld bc, 5
	ldir
	ex af, af'
	ld hl, (NEWPOS)
SPCLP1:
	push hl
	ld a, (SKIP)
	ld c, a
	add hl, bc
	bit 0, h
	jr z, NSSPS
	ld a, h
	add a, 7
	ld h, a
NSSPS:
	ld a, (FILL)
	and a
	jr z, NXTSPC
	ld c, a
SPCLP2:
	push hl					; Restore char
		ld b, 8
SPCLP3:
		ld a, (hl)			; Store screen first
		ld (de), a
		inc de
		inc h
		djnz SPCLP3
	pop hl
		inc hl				; Next char
		dec c
		jr nz, SPCLP2
NXTSPC:
	pop hl
	ex af, af'				; Update row count
	dec a
	jr z, SPCATR			; Restore police attr
	ex af, af'
	ld c, 32
	sbc hl, bc				; Up one line
	bit 0, h				; Cross screen section?
	jr z, SPCLP1
	ld a, h
	sub 7
	ld h, a
	jr SPCLP1
SPCATR:
	ld hl, (ATTPOS)			; Attribute start position
	ld a, (ROW)
	ex af, af'
SPCAT1
	push hl
	ld a, (SKIP)
	ld c, a
	add hl, bc
	ld a, (FILL)
	and a
	jr z, NXTSPA
	ld c, a
	ldir
NXTSPA:
	pop hl
	ex af, af'
	dec a
	ret z
	ex af, af'
	ld c, 32
	sbc hl, bc
	jr SPCAT1

RESPC:
	ld a, (PCAREXT)			; Test PC exist
	and a
	ret z
	ld de, ROW
	ld hl, PCSTR + 2
	ld bc, 5
	ldir					; Retrieve 5 info
	ex de, hl				; DE Storage PTR
	ld hl, (PCSTR)			; Load POS
	ld a, (ROW)
	ex af, af'
RPCLP1:
	push hl					; Save POS
	ld a, (SKIP)
	ld c, a
	add hl, bc
	bit 0, h
	jr z, NSRPS
	ld a, 7
	add a, h
	ld h, a
NSRPS:
	ld a, (FILL)
	and a
	jr z, NXTRPC
	ld c, a
RPCLP2:
	push hl
	ld b, 8
RPCLP3:
	ld a, (de)				; Restore char
	ld (hl), a
	inc de
	inc h
	djnz RPCLP3
	pop hl
	inc hl
	dec c
	jr nz, RPCLP2
NXTRPC:
	pop hl
	ex af, af'
	dec a					; Update row count
	jr z, RPCATR			; Restore police car
	ex af, af'
	ld c, 32
	sbc hl, bc				; Move up one line
	bit 0, h
	jr z, RPCLP1
	ld a, h
	sub 7					; Cross boundary
	ld h, a
	jr RPCLP1
RPCATR:
	ld hl, (ATTPOS)			; ATTR Start loading position
	ld a, (ROW)
	ex af, af'
RPCAT1:
	push hl
	ld a, (SKIP)
	ld c, a
	add hl, bc
	ld a, (FILL)
	and a
	jr z, NXTRPA
	ex de, hl
	ld c, a
	ldir
	ex de, hl
NXTRPA:
	pop hl
	ex af, af'
	dec a
	ret z
	ex af, af'
	ld c, 32
	sbc hl, bc
	jr RPCAT1

FROG:
	ld a, (CRHFLG)			; Crash flag
	and a
	jr nz, FRGCRH			; Frog crash
	ld (UPDWN), a			; Set no score
	call REGFRG				; Regenerate frog
	ld hl, FRGCYC			; Test move
	dec (hl)
	ret nz
	dec hl
	ld a, (hl)				; Reset cycle count
	inc hl
	ld (hl), a
	call MOVFRG
	ld a, (CRHFLG)
	and a
	ret z
FRGCRH:
	call CRASH
	ret

;********************************************
; REGFRG
;
; Regenerate frog if any left
; Set GAMFLG to 0 if none left
;********************************************
REGFRG:
	ld a, (FRGEXT)
	and a
	ret nz					; Return if exist
	ld hl, FRGDB
	ld de, FRGEXT
	ld bc, 8
	ldir
	ld hl, FRGSTN			; Update frog station
	dec (hl)				; Move 3 characters left
	dec (hl)
	dec (hl)
	ld hl, (FRGPOS)
	ld (OLDFRG), hl
	ld (NEWFRG), hl
	ld hl, FRGSTR			; Init FRG STR for RES
	ld de, FRGSTR+1			; Blank frog store
	ld bc, 35
	ld (hl), 0
	ldir
	ret

;********************************************
; MOVFRG
;
; Move frog, store and restore
;********************************************
MOVFRG:
	xor a
	ld hl, $e020			; H=-32; L=32
	ld c, a					; C => ABS Movement
	ex af, af'
	ld a, $df				; Test right
	in a, ($fe)
	and 1
	jr nz, LEFT
	inc c
	ld de, FROG2
	ld b, 1
LEFT:
	ld a, $df				; Test left
	in a, ($fe)
	and 4
	jr nz, DOWN
	dec c
	ld de, FROG4
	ld b, 3
DOWN:
	ld a, $fd				; Test down
	in a, ($fe)
	and 1
	jr nz, UP
	ld a, c
	add a, l				; add 32
	ld c, a
	ex af, af'
	dec a
	ex af, af'				; Dec up/down flag
	ld de, FROG3
	ld b, 2
UP:
	ld a, $f7				; Test up
	in a, ($fe)
	and 1
	jr nz, VALID
	ld a, c
	add a, h				; Add -32
	ld c, a
	ex af, af'
	inc a
	ex af, af'
	ld de, FROG1
	ld b, 0
VALID:
	ld a, b					; Store temp dir
	ld (TEMDIR), a
	ld (TEMSHP), de			; Store temp shape
	xor a
	cp c
	ret z					; If no move go back
	ld hl, (OLDFRG)
	bit 7, c				; Test -VE
	ld b, a
	ld e, 7					; For boundary adj
	jr z, NETDWN			; Net move RHT, DWN
	dec b
	ld e, -7
NETDWN:
	add hl, bc
	bit 0, h
	jr z, VALID1			; No cross boundary
	ld a, h
	add a, e
	ld h, a					; Adj HOB
VALID1:
	ld (TEMPOS), hl
	ex de, hl
	ld a, $40				; Test UPSCR
	cp d
	ld a, e
	jr nz, VALID2
	cp $20
	jr c, NVALID
VALID2:
	and $1f					; Test right boundary
	cp $1f
	jr z, NVALID
	ld hl, $50be			; Test bot boundary
	and a
	sbc hl, de
	jr c, YVALID
	ld a, e					; Test within box
	and $1f
	ld h, a
	ld a, (FRGSTN)
	cp $a0					; Test last frog
	jr c, YVALID			; No more frog station
	inc a					; When no frog left
	and $1f
	sub h
	jr nc, NVALID
YVALID:
	ld (NEWFRG), de			; Store new pos
	ex af, af'
	ld (UPDWN), a
	ex af, af'
NVALID:
	ld hl, (OLDFRG)			; Test OLDFRG=NEWFRG
	and a
	sbc hl, de
	ld a, l
	or h
	ret z					; Return if same
	call RESFRG				; Restore frog
	ld hl, (NEWFRG)			; Update old frog pos
	ld (OLDFRG), hl
	ld hl, TEMDIR
	ld de, FRGDIR
	ld bc, 5
	ldir
	call STRFRG
	ret

RESFRG:
	ld de, FRGSTR			; Storage PTR
	ld hl, (OLDFRG)			; Restore from OLDPOS
	push hl
	ld a, 2					; Row counter
	ex af, af'
RFRLP1:
	push hl
	ld c, 2					; Column counter
RFRLP2:
	push hl
	ld b, 8
RFRLP3:
	ld a, (de)				; Restore from DB
	ld (hl), a				; onto screen
	inc de
	inc h					; Next char byte
	djnz RFRLP3
	pop hl
	inc hl
	dec c					; Column count
	jr nz, RFRLP2
	pop hl
	ex af, af'
	dec a					; Row count
	jr z, RFRATR
	ex af, af'
	and a
	ld c, 32				; Up one line
	sbc hl, bc
	bit 0, h
	jr z, RFRLP1
	ld a, h
	sub 7
	ld h, a
	jr RFRLP1
RFRATR:
	pop hl
	ld a, h
	and $18
	sra a
	sra a
	sra a
	add a, $58
	ld h, a
	ld a, 2					; Row counter
	ex af, af'
RFRAT1:
	push hl
	ex de, hl
	ld c, 2					; Restore ATTR
	ldir
	ex de, hl
	pop hl
	ef af, af'
	dec a					; Update row counter
	ret z
	ex af, af'
	ld c, 32
	sbc hl, bc
	jr RFRAT1

STRFRG:
	ld de, FRGSTR
	ld hl, (NEWFRG)			; Store base on newpos
	exx
	ld hl, (FROGSH)			; Load shape as well
	exx
	push hl
	ld a, 2
	ex af, af'
SFRLP1:
	push hl
	ld c, 2
SFRLP2:
	push hl
	ld b, 8
SFRLP3:
	ld a, (hl)
	ld (de), a
	exx
	ld a, (hl)
	inc hl
	exx
	ld (hl), a
	inc de
	inc h
	djnz SFRLP3
	pop hl
	inc hl					; Next char
	dec c
	jr nz, SFRLP2
	pop hl
	ex af, af'
	dec a
	jr z, SFRATR
	ex af, af'
	and a
	ld c, 32
	sbc hl, bc				; Next row
	bit 0, h
	jr z, SFRLP1
	ld a, h
	sub 7
	ld h, a
	jr SFRLP1
SFRATR:
	pop hl
	ld a, h					; Calculate ATTR POS
	and $18
	sra a
	sra a
	sra a
	add a, $58
	ld h, a
	ld a, 2
	ex af, af'
SFRAT1:
	ld b, 2
	push hl
SFRATLP:
	ld a, (hl)
	ld (de), a
	ld (hl), 4				; Fill Frog ATTR
	inc hl
	inc de
	and 7					; Test CRASH
	jr zm NFROG3
	ld a, 1
	ld (CRHFLG), a
NFROG3:
	djnz SFRATLP
	pop hl
	ex af, af'
	dec a
	ret z
	ex af, af'
	ld c, 32
	sbc hl, bc
	jr SFRAT1

CRASH:
	xor a
	ld (CRHFLG), a			; Reset Crash flag
	ld (FRGEXT), a			; Set frog nonexist
	call FRGDIE				; Frog dying routine
	call RESFRG
	ld hl, NUMFRG
	dec (hl)				; Decrease Frog number
	ret nz
	ld (GAMFLG), a			; Zeroise game flag
							; when no frog left

FRGDIE:
	ld hl, (OLDFRG)			; Old position of frog
	ld bc, $4002			; Red colour
	exx
	ld hl, DIETON			; Set Die tone
	exx
	ld a, h					; Test end of journey
	cp b
	jr nz, NOTEND
	ld a, l
	cp b
	jr nc, NOTEND
	ld de, SCORE+3			; 100 points bonus
	ex de, hl
	inc (hl)
	ld hl, SCORE+4
	call DISSCR
	ld c, 6					; Yellow
	exx
	ld hl, HOMTON
	exx
NOTEND:
	ld a, c
	ld (ATTR), a
	ld hl, (OLDFRG)
	ld de, (FROGSH)
	call DRWFRG
	ld de, 32				; Line adjust
	add hl, de
	ex af, af'
	ld a, (ATTR)
	ex af, af'
	ld b, 5
FLASLP:
	push bc
	push hl					; Attribute PTR
	xor a					; Black ink; Black paper
	ld (hl), a
	inc hl
	ld (hl), a
	sbc hl, de
	ld (hl), a
	dec hl
	ld (hl), a
	call FRGTON				; Generate Frog Tone
	pop hl
	push hl
	ex af, af'
	ld (hl), a				; Black paper, red or
	inc hl					; yellow ink
	ld (hl), a
	and a
	sbc hl, de
	ld (hl), a
	dec hl
	ld (hl), a
	ex af, af'
	call FRGTON
	pop hl
	pop bc
	djnz FLASLP
	ret

FRGTON:
	exx
	push hl
	call TONE1
	pop hl
	ld bc, 4				; Move down database
	ex af, af'
	cp 6
	jr z, HOME
	ld bc, -4				; Move up database
HOME:
	add hl, bc
	exx
	ex af, af'
	ret

CALSCR:
	ld a, (FRGEXT)			; Test existence
	and a
	ret z					; No update of score
	ld a, (UPDWN)			; Test up/down movement
	and a					; Test any score
	ret z
	ld hl, SCORE+4			; Add 10 to score
	bit 7, a				; Test move down
	jr nz, DWNSCR			; Down score
	inc (hl)
	jr DISSCR				; Dis score
DWNSCR:
	ld a, (OLDFRG+1)		; Test HOB
	cp $40					; Test first block
	jr nz, TLHWY			; Test low highway
	ld a, (OLDFRG)
	cp $c0					; Not even step on HWY
	ret c
	inc (hl)
	jr DISSCR
TLHWY:
	cp $50					; Test in low HWY
	ret nz
	ld a, (OLDFRG)
	cp $20
	ret nc					; No score if step HWY
	inc (hl)
DISSCR:
	ld b, 4					; HL => Tenth's POS
ADDLOP:
	ld a, (hl)
CRYLOP:
	cp $3a					; Carry loop
	jr c, UPDDIG			; Update digit
	sub 10
	dec hl
	inc (hl)				; Carry
	inc hl
	jr CRYLOP
UPDDIG:
	ld (hl), a
	dec hl
	djnz ADDLOP
	ld hl, SCORE+1
	call SCRIMG				; Score image
	ld hl, $4006
	ld de, IMAGE
	ld b, 5
	call DISASC
	ret

SCRIMG:
	ld de, IMAGE
	ld bc, 5
	ldir
	ld hl, IMAGE
	ld bc, $0430
PREZER:
	ld a, c
	cp (hl)					; Test $30
	jr nz, PREZEX
	ld (hl), $20			; Space fill
	inc hl
	djnz PREZER
PREZEX:
	ret

SIREN:
	ld a, $bf
	in a, ($fe)
	and 1
	jr nz, NSOUND
	ld a, (SOUNDF)			; Reset sound condition
	inc a
	and 1
	ld (SOUNDF), a
NSOUND:
	ld a, (SOUNDF)
	and a
	jr z, DELAY
	ld a, (CHASE)			; Is police car on?
	and a
	jr z, DELAY
	ld a, (TONFLG)
	inc a
	and 1
	ld (TONFLG), a
	ld hl, PCTON1
	jr z, TONE1
	ld hl, PCTON2
TONE1:
	ld e, (hl)				; DE = Duration * Frequency
	inc hl
	ld d, (hl)
	inc hl
	ld c, (hl)
	inc hl
	ld b, (hl)
	push bc
	pop hl					; HL = 437500 / FREQ-30.125
	call $03b5
	di						; $03b5 enable interrupt
	ret
DELAY:
	ld bc, 6144
WAIT:
	dec bc
	ld a, b
	or c
	jr nz, WAIT
	ret

RANDNO:
	push hl
	push bc
	ld hl, (RND)
	ld b, (hl)
	inc hl
	ld a, $3f				; Bound pointer within row
	and h
	ld h, a
	ld a, b
	ld (RND), hl
	pop bc
	pop hl
	ret

OVER:
	ld hl, SCORE+1			; High score manage
	ld de, HISCR
	ld b, 5
SORTLP:
	ld a, (de)
	cp (hl)
	jr z, SAMSCR			; Test 1st NE digit
	ret nc
	jr SCRGT				; Update high score
SAMSCR:
	inc de
	inc hl
	djnz SORTLP
	ret
SCRGT:
	ld hl, SCORE+1
	ld de, HISCR
	ld bc, 5
	ldir
	ret

FINAL:
	ld a, 56				; Set white border
	ld (23624), a
	ld hl, $4000			; Start of screen
	ld de, $4001
	ld bc, 6143				; Size of screen
	ld (hl), 0
	ldir
	ld hl, $5800			; Start of attribute file
	ld de, $5801
	ld bc, 767
	ld (hl), 56				; White paper, black ink
	ldir
	ret

	


	

	


